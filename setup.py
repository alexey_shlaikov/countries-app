# -*- coding: utf-8 -*-

import getpass
import vk_api

from app.bottle import Server


def start_server(session):
    server = Server(
        host='localhost',
        port=8080,
        session=session
    )
    server.start()


def auth_handler():
    key = input('Enter authentication code: ')
    remember_device = False

    return key, remember_device


def main():
    print('Enter your login and password...\n')
    login = input('Login: ')
    password = getpass.getpass()
    vk_session = vk_api.VkApi(
        login, password,
        auth_handler=auth_handler
    )
    start_server(vk_session)

if __name__ == '__main__':
    main()
