from bottle import Bottle, SimpleTemplate, HTTPError, static_file


class Server:
    def __init__(self, host, port, session):
        self._host = host
        self._port = port
        self._session = session
        self._api = self._vk_session()
        self._app = Bottle()
        self._routes()
        self._base_static()

    def _vk_session(self):
        try:
            self._session.auth()
        except:
            raise HTTPError(500, "HTTP Error: Сan't connect  to the VK.com")
        return self._session.get_api()

    def _template(self, name, data=None):
        tpl_dir = 'app/views/' + name + '.tpl'
        template = SimpleTemplate(name=tpl_dir)
        if(data != None):
            return template.render(data=data)
        return template.render()

    def _routes(self):
        self._app.route('/static/:fname#.*#', callback=self._static)
        self._app.route('/', method="GET", callback=self._index)
        self._app.route('/cities/<id>', callback=self._cities)

    def _static(self, fname):
        return static_file(fname, root='app/static/')

    def _base_static(self):
        self._static('favicon.ico')
        self._static('styles/bootstrap.min.css')
        self._static('styles/main.css')

    def _index(self):
        response = self._api.database.getCountries(count=300, need_all=1)
        return self._template('index', data=response['items'])

    def _cities(self, id):
        response = self._api.database.getCities(country_id=id, count=1000, need_all=0)
        self._static('scripts/sort.js')
        return self._template('cities', data=response['items'])

    def start(self):
        self._app.run(
            host=self._host,
            port=self._port,
            debug=False,
            reloader=False
        )
