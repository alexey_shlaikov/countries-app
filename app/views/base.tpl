<!doctype html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Города-страны">
    <meta name="author" content="Alexey Shlaikov">
    <link rel="icon" href="/static/favicon.ico">

    <title>{{title or 'Города-страны'}}</title>

    <link href="/static/styles/bootstrap.min.css" rel="stylesheet">
    <link href="/static/styles/open-iconic-bootstrap.min.css" rel="stylesheet">
    <link href="/static/styles/main.css" rel="stylesheet">
  </head>

  <body>
    <div class="container">

      <header class="header clearfix">
        <h3 class="text-muted">Города-страны</h3>
      </header>

      {{!base}}

      <footer class="footer">
        <p>&copy; Города-страны, 2018</p>
      </footer>

      <script src="/static/scripts/jquery.slim.min.js"></script>
      <script src="/static/scripts/popper.min.js"></script>
      <script src="/static/scripts/bootstrap.min.js"></script>
      <script src="/static/scripts/sort.js"></script>

    </div>
  </body>
</html>