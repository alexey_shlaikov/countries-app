% rebase('app/views/base.tpl', title='Города')
<main role="main">
    <div class="jumbotron">
        <div class="row jumbotron-menu">
            <div class="col-sm">
                <h1 class="display-3">Список городов</h1>
            </div>
            <div class="col-sm" style="text-align: end;">
                <button id="sortButton" type="button" class="btn btn-secondary" title="Сортировать по возрастанию">
                    <span class="oi oi-sort-ascending" id="io-icon"></span>
                </button>
            </div>
        </div>
        <ul id="list-cities" class="list-group">
        % for item in data:
            <li class="list-group-item" city-id="{{item['id']}}">
                {{item['title']}}
            </li>
        % end
        </ul>
    </div>
</main>