% rebase('app/views/base.tpl', title='Страны')
<main role="main">
    <div class="jumbotron">
        <h1 class="display-3">Список стран</h1>
        <ul class="list-group">
        % for item in data:
            <li class="list-group-item" country-id="{{item['id']}}">
                <a href="/cities/{{item['id']}}">{{item['title']}}</a>
            </li>
        % end
        </ul>
    </div>
</main>